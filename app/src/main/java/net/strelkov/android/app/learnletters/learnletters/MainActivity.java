package net.strelkov.android.app.learnletters.learnletters;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.storage.StorageManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.FileStore;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSink;
import okio.Utf8;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static final int PERMISSION_REQUEST_CODE = 200;

    public MySurfaceView myView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);

        //setContentView(new MySurfaceView(this));
        setContentView(R.layout.activity_main);

        //ImageView img = (ImageView) findViewById(R.id.imageView);
        //MySurfaceView view =  ;
        LinearLayout layout = (LinearLayout) findViewById(R.id.verLayout);
        myView = new MySurfaceView(this);
        layout.addView(myView);

        Button btnBack = (Button) findViewById(R.id.btnBack);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.finish();
            }
        });

        Button btnSave = (Button) findViewById(R.id.btnSave);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isGrantExternalRW(MainActivity.this)) {
                    SaveImage(setViewToBitmapImage(myView), "to_pavel.jpg");
                }
            }
        });

        //reset
        Button btnReset = (Button) findViewById(R.id.btnReset);
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myView.path.reset();
                myView.refreshDrawableState();
            }
        });
        //send
        Button btnSend = (Button) findViewById(R.id.btnSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String filePath = SaveImage(setViewToBitmapImage(myView), "to_pavel.jpg");

                FileUploadService service = ServiceGenerator.createService(FileUploadService.class);

                MultipartBody.Part body = prepareFilePart("photo", filePath);

                Call<Predict> call = service.upload(body);

                call.enqueue(new Callback<Predict>() {
                    @Override
                    public void onResponse(Call<Predict> call,
                                           Response<Predict> response) {
                        int duration = Toast.LENGTH_LONG;

                        Predict predict = response.body();

                        Toast toast = Toast.makeText(MainActivity.this, predict.getLetter(), duration);
                        toast.show();

                        System.out.println("success");
                    }

                    @Override
                    public void onFailure(Call<Predict> call, Throwable t) {
                        System.out.println("Upload error");

                    }
                });


            }
        });
        //img.setImageDrawable(new MySurfaceView(this, img));

    }

    class MySurfaceView extends View {

        private final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        private final Path path;

        public MySurfaceView(Context context) {
            super(context);
            path = new Path();
            //paint = img.getRootView();
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(40);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeCap(Paint.Cap.ROUND);
            paint.setColor(Color.RED);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            int action = event.getAction();
            if (action == MotionEvent.ACTION_DOWN) {
                path.moveTo(event.getX(), event.getY());
            } else if (action == MotionEvent.ACTION_MOVE) {
                path.lineTo(event.getX(), event.getY());
            } else if (action == MotionEvent.ACTION_UP) {
                path.lineTo(event.getX(), event.getY());
            }
            invalidate();
            return true;
        }

        @Override
        protected void onDraw(Canvas canvas) {
            if (path != null) canvas.drawPath(path, paint);
        }
    }

    public static Bitmap setViewToBitmapImage(View view) {
//Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null) {
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
            System.out.println("canvas");

        } else {
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
            System.out.println("WHITE");

        }
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }

    public static String SaveImage(Bitmap finalBitmap, String filename) {

        String root = Environment.getExternalStorageDirectory().toString();

        String baseDir = Environment.getExternalStorageDirectory().getAbsolutePath();
        String fileName = "myFile.jpg";

        // Not sure if the / is on the path or not
        File f = new File(baseDir + File.separator + fileName);

        System.out.println(baseDir + File.separator + fileName);

        // File file = new File(root, filename);
        try {
            FileOutputStream out = new FileOutputStream(f);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            return baseDir + File.separator + fileName;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static boolean isGrantExternalRW(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (context.checkSelfPermission(
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {

            ((Activity) context).requestPermissions(new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            }, PERMISSION_REQUEST_CODE);

            return false;
        }

        return true;
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, descriptionString);
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, String path) {
        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
        // use the FileUtils to get the actual file by uri
        File file = new File(path);

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("image/jpeg"),
                        file
                );

        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }
}

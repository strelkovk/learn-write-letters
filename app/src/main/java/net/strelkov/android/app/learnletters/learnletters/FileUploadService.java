package net.strelkov.android.app.learnletters.learnletters;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface FileUploadService {
    @Multipart
    @POST("predict")
    Call<Predict> upload(
            @Part MultipartBody.Part photo
    );
}
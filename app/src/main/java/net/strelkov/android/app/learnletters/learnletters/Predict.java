package net.strelkov.android.app.learnletters.learnletters;

public class Predict {

    private String letter;

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }
}